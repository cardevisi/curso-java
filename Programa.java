class Programa {
	public static void main(String[] args) {
		int idadeCarlos = 38;
		int idadeAna = 29;
		int somaDasIdades = idadeCarlos + idadeAna;
		System.out.println("A soma da idades e igual a:"+somaDasIdades);	

		double pi = 3.1415;
		System.out.println("O valor de PI e igual:"+pi);

		double outroPi = 10.8;
		float piF = (float ) outroPi; //Casting de variaveis

		boolean amigo = true;
		boolean inimigo = false;
		System.out.println("Amigo:"+amigo);	
		System.out.println("Inimigo:"+inimigo);	

		char letra = 'M';
		String nomeCompleto = "João da Silva";

		System.out.println("Letra:"+letra);	
		System.out.println("Nome Completo:"+nomeCompleto);

		long numero = 314;
		int copiaDeNumero = (int) numero;
	}
}